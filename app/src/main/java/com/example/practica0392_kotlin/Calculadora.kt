package com.example.practica0392_kotlin

class Calculadora {
    var num1:Int = 0
    var num2:Int = 0

    constructor(num1:Int, num2:Int){
        this.num1 = num1
        this.num2 = num2
    }

    fun suma():Int {
        return this.num1 + this.num2
    }

    fun resta():Int {
        return this.num1 - this.num2
    }

    fun multiplicacion():Int {
        return this.num1 * this.num2
    }

    fun divicion():Int {
        var total = 0

        if(this.num2 != 0)
            total = this.num1/this.num2

        return total
    }
}