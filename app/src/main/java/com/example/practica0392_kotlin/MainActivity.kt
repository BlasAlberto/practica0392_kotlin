package com.example.practica0392_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.Touch
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    // Declaración de componenetes
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.iniciarComponentes()
        btnIngresar.setOnClickListener { this.ingresar() }
        btnSalir.setOnClickListener { this.salir() }
    }


    // Enlazar componentes con los del Layout
    private fun iniciarComponentes(){
        this.btnIngresar = findViewById(R.id.btnIngresar) as Button
        this.btnSalir = findViewById(R.id.btnSalir) as Button
        this.txtUsuario = findViewById(R.id.txtUsuario) as EditText
        this.txtContraseña = findViewById(R.id.txtContraseña) as EditText
    }


    private fun ingresar(){
        var strUsuario : String
        var strContraseña : String

        strUsuario = applicationContext.resources.getString(R.string.usuario)
        strContraseña = applicationContext.resources.getString(R.string.contraseña)
        // Para java es el siguiente
        // strUsuario = applicationContext.getResources.getString(R.string.usuario)

        if(strUsuario.toString().equals(txtUsuario.text.toString()) && strContraseña.toString().equals(txtContraseña.text.toString())){

            // Hacer el paquete para enviar la información
            var bundle = Bundle()
            bundle.putString("Usuario", txtUsuario.text.toString())

            // Hacer el intent para llamar otra actividad
            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando o no respuesta
            startActivity(intent)

            this.txtUsuario.setText("")
            this.txtContraseña.setText("")
        }
        else{
            Toast.makeText(this.applicationContext, "El usuario o contraseña no es valido.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Decea cerrar la aplicación?")

        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }

        confirmar.show()
    }
}