package com.example.practica0392_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class CalculadoraActivity : AppCompatActivity() {

    // Declaración de componentes
    private lateinit var btnSumar:Button
    private lateinit var btnRestar:Button
    private lateinit var btnMultiplicar:Button
    private lateinit var btnDividir:Button
    private lateinit var btnLimpiar:Button
    private lateinit var btnRegresar:Button
    private lateinit var lblUsuario: TextView
    private lateinit var lblResultado: TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    // Declaración objeto Calculadora
    private var calculadora = Calculadora(0, 0)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        this.iniciarComponentes()

        this.btnSumar.setOnClickListener { this.btnSumar() }
        this.btnRestar.setOnClickListener { this.btnResta() }
        this.btnMultiplicar.setOnClickListener { this.btnMultiplicacion() }
        this.btnDividir.setOnClickListener { this.btnDivicion() }
        this.btnLimpiar.setOnClickListener { this.btnLimpiar() }
        this.btnRegresar.setOnClickListener { this.btnRegresar() }

        // Obtener los datos del MainActivity
        var datos = intent.extras
        var usuario = datos!!.getString("Usuario")
        this.lblUsuario.text = usuario.toString()
    }



    // Relación de componentes con el layout
    private fun iniciarComponentes(){
        this.btnSumar = findViewById(R.id.btnSumar) as Button
        this.btnRestar = findViewById(R.id.btnRestar) as Button
        this.btnMultiplicar = findViewById(R.id.btnMultiplicar) as Button
        this.btnDividir = findViewById(R.id.btnDividir) as Button
        this.btnLimpiar = findViewById(R.id.btnLimpiar) as Button
        this.btnRegresar = findViewById(R.id.btnRegresar) as Button

        this.lblUsuario = findViewById(R.id.lblUsuario) as TextView
        this.lblResultado = findViewById(R.id.lblResultado) as TextView

        this.txtNum1 = findViewById(R.id.txtNum1) as EditText
        this.txtNum2 = findViewById(R.id.txtNum2) as EditText
    }

    // Metodo clic para el Button: btnSumar
    fun btnSumar(){
        if(!txtNum1.text.toString().equals("") && !txtNum2.text.toString().equals("")){
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()

            var total = calculadora.suma()

            lblResultado.text = total.toString()
        }
        else Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_SHORT).show()

    }

    // Metodo clic para el Button: btnRestar
    fun btnResta(){
        if(!txtNum1.text.toString().equals("") && !txtNum2.text.toString().equals("")){
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()

            var total = calculadora.resta()

            lblResultado.text = total.toString()
        }
        else Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_SHORT).show()
    }

    // Metodo clic para el Button: btnMultiplicacion
    fun btnMultiplicacion(){
        if(!txtNum1.text.toString().equals("") && !txtNum2.text.toString().equals("")){
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()

            var total = calculadora.multiplicacion()

            lblResultado.text = total.toString()
        }
        else Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_SHORT).show()
    }

    // Metodo clic para el Button: btnDivicion
    fun btnDivicion(){
        if(!txtNum1.text.toString().equals("") && !txtNum2.text.toString().equals("")){
            calculadora.num1 = txtNum1.text.toString().toInt()
            calculadora.num2 = txtNum2.text.toString().toInt()

            var total = calculadora.divicion()

            lblResultado.text = total.toString()
        }
        else Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_SHORT).show()
    }

    // Metodo clic para el Button: btnLimpiar
    fun btnLimpiar(){
        lblResultado.setText("")
        txtNum1.setText("");
        txtNum2.setText("");
    }

    // Metodo clic para el Button: btnRegresar
    fun btnRegresar(){
        var confirmar = AlertDialog.Builder(this)

        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Decea cerrar seción?")

        confirmar.setPositiveButton("Confirmar"){
            dialogInterface, which->finish()
        }

        confirmar.setNegativeButton("Cancelar"){
            dialogInterface, which->
        }

        confirmar.show()
    }
}